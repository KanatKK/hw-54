import React from 'react';

const Reset = props => {
    return (
            <button type="button" className="btn" onClick={props.btnClick}>Reset</button>
    );
};

export default Reset;