import React from 'react';

const Tries = props => {
    return (
            <p className="tries">Tries: {props.tries}</p>
    );
};

export default Tries;