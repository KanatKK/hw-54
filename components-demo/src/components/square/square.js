import React from 'react';

const Square = props => {

    return (
        <div style={{background: props.white}} className="square" id={props.id} onClick={props.onSquareClick}><p className="star">{props.star}</p></div>
    );
};

export default Square;