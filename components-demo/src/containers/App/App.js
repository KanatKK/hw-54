import React, {useState} from 'react';
import './App.css';
import '../../components/square/square'
import Square from "../../components/square/square";
import Tries from "../../components/Tries/tries";
import Reset from "../../components/reset/reset";

const App = () => {
    let [squares] = useState([]);
    const [tries, setTries] = useState([{tries: -1}])

    const addSquare = () => {
        for (let i = 1; i <= 36; i++) {
            squares.push({ hasItem: false , star: "", id: i, white: ""})
        }
        for (let key in squares) {
            squares[key].hasItem = false
            squares[key].star = ""
        }
        const getRandom = Math.floor(Math.random()*36)+0
        squares[getRandom].hasItem= true
        for (let key in squares) {
            if(squares[key].hasItem) {
                squares[key].star = "*"
            }
        }
    }
    if (squares.length === 0) {
        addSquare()
    }

    const newTries = () => {
        const triesCopy = [...tries]
        tries[0].tries++
        setTries(triesCopy)
    }

    const changeColor = () => {
        let clickedSquare = document.querySelectorAll('.square')
        let clicked = []
        for (let i = 0; i < clickedSquare.length; i++){
            clicked.push(clickedSquare[i]);
            clickedSquare[i].addEventListener('click', function (e) {
                squares[clicked.indexOf(e.target)].white = "white"
            })
        }
    }
    changeColor()

    const reset = () => {
        squares.length = 0;
        tries[0].tries = 0;
        addSquare();
    }

    let squareList = squares.map((squares, index) => {
        return(
            <Square  star={squares.star} hasItem={squares.hasItem} key={index} id={squares.id} white={squares.white} onSquareClick={newTries}/>
        )
    });
    return(
        <div className="container">
            <div className="field">
                <div className="squares">
                    {squareList}
                </div>
            </div>
            <Tries tries={tries[0].tries}></Tries>
            <Reset btnClick={reset}/>
        </div>
    )
};

export default App;